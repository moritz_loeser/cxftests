package com.sas;

import org.apache.camel.builder.RouteBuilder;

import com.sas.o2.rtdm.general.EventPortType;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    /**
     * Let's configure the Camel routing rules using Java code...
     */
    @Override
    public void configure() {
        from("file:C:/Users/moritz/entwicklung/telefonica/integration-new/pcrf?noop=true").process(new GenerateEvent())
        .to("cxf:http://localhost:8088/mockEventSoapBinding"+ "?serviceClass=" + EventPortType.class.getCanonicalName());
        // here is a sample which processes the input files
        // (leaving them in place - see the 'noop' flag)
        // then performs content based routing on the message using XPath
    }

}
