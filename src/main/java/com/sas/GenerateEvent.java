package com.sas;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.sas.o2.rtdm.general.types.BodyType;
import com.sas.o2.rtdm.general.types.EventType;
import com.sas.o2.rtdm.general.types.HeaderType;
import com.sas.o2.rtdm.general.types.ObjectFactory;

public class GenerateEvent implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectFactory objectFactory = new ObjectFactory();
        EventType event = objectFactory.createEventType();
        event.setName("someTest");
        HeaderType header = objectFactory.createHeaderType();
        event.setHeader(header);
        BodyType body = objectFactory.createBodyType();
        event.setBody(body);
        exchange.getOut().setBody(event);
    }

}
